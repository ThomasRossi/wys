<?php
global $_service;
$_config = $_service->get('config');
?>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $_config['website_title']?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="author" content="" />

<!-- Facebook and Twitter integration -->
<meta property="og:title" content=""/>
<meta property="og:image" content=""/>
<meta property="og:url" content=""/>
<meta property="og:site_name" content=""/>
<meta property="og:description" content=""/>
<meta name="twitter:title" content="" />
<meta name="twitter:image" content="" />
<meta name="twitter:url" content="" />
<meta name="twitter:card" content="" />

<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<link rel="shortcut icon" href="favicon.ico">

<!-- Google Webfonts -->
<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

<!-- Animate.css -->
<link rel="stylesheet" href="css/animate.css">
<!-- Icomoon Icon Fonts-->
<link rel="stylesheet" href="css/icomoon.css">
<!-- Owl Carousel -->
<link rel="stylesheet" href="css/owl.carousel.min.css">
<link rel="stylesheet" href="css/owl.theme.default.min.css">
<!-- Magnific Popup -->
<link rel="stylesheet" href="css/magnific-popup.css">
<!-- Theme Style -->
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/bootstrap-social.css">
<script src="https://use.fontawesome.com/da68880c38.js"></script>
<!-- Modernizr JS -->
<script src="js/modernizr-2.6.2.min.js"></script>
<!-- FOR IE9 below -->
<!--[if lt IE 9]>
<script src="js/respond.min.js"></script>
<![endif]-->

<!-- Instagram gallery style -->
<script src="js/masonry.min.js"></script>
<script src="js/infinite-scroll.min.js"></script>
<!-- End instagram gallery -->

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>

<style>
    .owl-carousel-fullwidth.owl-theme .owl-dots {
        margin-bottom: 195em;
    }

    .slide {
        width: 100%;
        background-repeat: round;
    }

    .owl-carousel-fullwidth .item .fh5co-overlay {
        background: #fff;
        opacity: 0;
    }

    .centerme {
        margin:auto;
    }

    .dontfloatme {
        float: none !important;
    }

    .w35 {
        width: 35%;
    }

    .owl-carousel .owl-item img {
        width: 50%;
        margin: auto;
    }

    .jet-li {
        list-style-type: none;
        margin-top: 30px;
    }

    .btn-circle {
        width: 30px;
        height: 30px;
        text-align: center;
        padding: 6px 0;
        font-size: 12px;
        line-height: 1.428571429;
        border-radius: 15px;
    }
    .btn-circle.btn-lg {
        width: 50px;
        height: 50px;
        padding: 10px 16px;
        font-size: 18px;
        line-height: 1.33;
        border-radius: 25px;
    }
    .btn-circle.btn-xl {
        width: 70px;
        height: 70px;
        padding: 10px 16px;
        font-size: 24px;
        line-height: 1.33;
        border-radius: 35px;
    }

    .btn-send {
        margin-left: 17% !important;
        margin-top: -7em !important;
        position: absolute !important;
        width: 200px !important;
        height: 200px !important;
        border-radius: 100px !important;
        color: #0633ff !important;
    }

    .btn-send::selection {
        color: #0633ff !important;
        background: #fff !important;
    }

    .btn-send:hover {
        color: #fff !important;
        background-color: #0633ff !important;
        border-color: #fff;
    }

    #f1_upload_process{
        z-index:100;
        position:absolute;
        visibility:hidden;
        text-align:center;
        width:400px;
        margin:0px;
        padding:0px;
        background-color:#fff;
        border:1px solid #ccc;
    }

</style>