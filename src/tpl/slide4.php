<?php
global $_service;
$_config = $_service->get('config');
?>
<div class="item slide4">
    <div class="fh5co-overlay"></div>
    <div class="container slide">
        <ul id="images">
            <?php
            $uploads = $_service->get('db')->exec(
                "SELECT * FROM uploads"
            );
            foreach($uploads as $upload)
            {
                print_r($upload);
            }
            ?>
            <!--<li class="jet-li">
                <a href="https://www.pexels.com/photo/mist-misty-fog-foggy-7919/">
                    <img class="gallery-images" src="https://pexels.imgix.net/photos/7919/pexels-photo.jpg?fit=crop&w=640&h=480" alt="" />
                </a>
            </li>
            <li class="jet-li">
                <a href="https://www.pexels.com/photo/landscape-nature-sunset-trees-479/">
                    <img class="gallery-images" src="https://pexels.imgix.net/photos/479/landscape-nature-sunset-trees.jpg?fit=crop&w=640&h=480" alt="" />
                </a>
            </li>
            <li class="jet-li">
                <a href="https://www.pexels.com/photo/landscape-sun-trees-path-21008/">
                    <img class="gallery-images" src="https://pexels.imgix.net/photos/21008/pexels-photo.jpg?fit=crop&w=640&h=480" alt="" />
                </a>
            </li>
            <li class="jet-li">
                <a href="https://www.pexels.com/photo/cold-snow-landscape-nature-1127/">
                    <img class="gallery-images" src="https://pexels.imgix.net/photos/1127/cold-snow-landscape-nature.jpg?fit=crop&w=640&h=480" alt="" />
                </a>
            </li>
            <li class="jet-li">
                <a href="https://www.pexels.com/photo/coastline-aerial-view-sea-9148/">
                    <img class="gallery-images" src="https://pexels.imgix.net/photos/9148/pexels-photo.jpeg?fit=crop&w=640&h=480" alt="" />
                </a>
            </li>-->
        </ul>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $(window).endlessScroll({
                inflowPixels: 300,
                callback: function() {
                    var $img = $('#images li:nth-last-child(5)').clone();
                    $('#images').append($img);
                }
            });
        });
    </script>
</div>



