<?php
global $_service;
$_config = $_service->get('config');
?>
<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <!-- Mobile Toggle Menu Button -->
            <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle" data-toggle="collapse" data-target="#fh5co-navbar" aria-expanded="false" aria-controls="navbar"><i></i></a>
            <a class="navbar-brand" href="index.html"></a>
        </div>
        <div id="fh5co-navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#"><span class="fa fa-cog"></span></a></li>
                <li><a href="#"><span>About <span class="border"></span></span></a></li>
            </ul>
        </div>
    </div>
</nav>