<?php
global $_service;
?>

<div class="item slide5">
    <div class="fh5co-overlay"></div>
    <div class="container slide profile" style="height:300px;background-image:url(images/users/<?php echo $_SESSION['username']?>.jpg)">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="fh5co-owl-text-wrap">
                    <div class="fh5co-owl-text text-center to-animate">
                        <a class="btn btn-default btn-circle btn-xl btn-send">
                            <p style="margin-top: 42%;">
                                Send
                            </p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container slide upload-process">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="fh5co-owl-text-wrap">
                    <div class="fh5co-owl-text text-center to-animate">
                        <img id="loading-spinner" class="loading-spinner" style="width:25%;" src="images/loader.gif" />
                        <img style="width:50%;" id="img-uploaded" class="img-uploaded" src="">
                        <form id="upload-file-form" action="finish.php" class="upload-file-form">
                            <input class="form-control input-lg file-uploader" type="file" name="myfile" id="file-uploader">
                            <br />
                            <textarea placeholder="Message" name="message" id="message" class="message form-control input-lg" rows="3"></textarea>
                            <br />
                            <select class="selectpicker">
                                <option>0,80€</option>
                                <option>3€</option>
                                <option>5€</option>
                            </select>
                            <br />
                            <a id="preview-btn" style="background-color: #0633ff;color: #fff;" class="form-control input-lg preview-btn">Preview</a>
                            <?php if(isset($_SESSION['payment_token']) && !empty($_SESSION['payment_token'])) { ?>
                                <button id="upload-btn" style="background-color: #0633ff;color: #fff;" type="submit" class="form-control input-lg upload-btn">Upload</button>
                            <?php }else{ ?>
                                <a id="upload-btn" style="background-color: #0633ff;color: #fff;" class="form-control input-lg upload-btn" data-toggle="modal" data-target="#payModal">Upload</a>
                            <?php } ?>
                        </form>
                        <iframe id="upload_target" name="upload_target" src="#" style="width:0;height:0;border:0px solid #fff;"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container slide thank-you">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="fh5co-owl-text-wrap">
                    <div class="fh5co-owl-text text-center to-animate">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        $('.btn-send').on('click', function(){
            $('.profile').hide();
            $('.upload-process').show();
        });
        $('.file-uploader').on('click', function(){
            $('.loading-spinner').hide();
        });
        $('.upload-process').hide();
        $('.thank-you').hide();
        $('.loading-spinner').hide();
        $('.upload-btn').hide();
        $('.selectpicker').hide();

        $('.close-paypal').on('click', function() {
            $('.upload-process').hide();
            $('.thank-you').show();
        });

        $('.preview-btn').on('click', function(){
            $('.preview-btn').hide();
            $('.loading-spinner').show();
            $('.file-uploader').hide();

            var form = $('.upload-file-form').get(1);
            var formData = new FormData();
            var file = $('.file-uploader').get(1);
            formData.append("myfile", file.files[0]);

            if(typeof formData != 'undefined' && formData)
            {
                $.ajax({
                    url: "upload.php", // Url to which the request is send
                    type: "POST",             // Type of request to be send, called as method
                    data: formData, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                    contentType: false,       // The content type used when sending data to the server.
                    cache: false,             // To unable request pages to be cached
                    processData:false,        // To send DOMDocument or non processed data file it is set to false
                    success: function(data)   // A function to be called if request succeeds
                    {
                        if(data)
                        {
                            $('.img-uploaded').attr('src', data);
                            $('.loading-spinner').hide();
                            $('.upload-btn').show();
                            $('.selectpicker').show();
                        }
                    }
                });
            }
        });


    </script>
</div>