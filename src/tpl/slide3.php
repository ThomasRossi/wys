<?php
global $_service;
$_config = $_service->get('config');
?>
<div class="item slide3">
    <div class="fh5co-overlay"></div>
    <div class="container slide" style="background-image:url(images/slide_3.jpg)">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="fh5co-owl-text-wrap">
                    <div class="fh5co-owl-text text-center to-animate">
                        <form action="dashboard" method="post" id="login-form">
                            <br />
                            <br />
                            <br />
                            <h1 class="text-center" style="color: #fff;">Register!</h1>
                            <h3 style="color: #fff;">And start share your happiness</h3>
                            <br />
                            <a class="btn btn-social btn-facebook w35 fake-face">
                                <span class="fa fa-facebook-square"></span> Sign in with Facebook
                            </a>
                            <br />
                            <a class="btn btn-social btn-bitbucket w35" data-toggle="modal" data-target="#myModal">
                                <span class="fa fa-pencil"></span> Sign in
                            </a>
                            <hr width="10%">
                            <input id="fake-face" name="fake-face" type="hidden" value="0">
                            <div class="col-md-6 centerme dontfloatme">
                                <div class="form-group">
                                    <label for="name" class="sr-only">Name</label>
                                    <input placeholder="Name" id="name" type="text" class="form-control input-lg">
                                </div>
                            </div>
                            <div class="col-md-6 centerme dontfloatme">
                                <div class="form-group">
                                    <label for="nickname" class="sr-only">Nickname</label>
                                    <input placeholder="Nickname" id="nickname" type="text" class="form-control input-lg">
                                </div>
                            </div>
                            <div class="col-md-6 centerme dontfloatme">
                                <div class="form-group">
                                    <label for="email" class="sr-only">Email</label>
                                    <input placeholder="Email" id="email" type="text" class="form-control input-lg">
                                </div>
                            </div>
                            <div class="col-md-6 centerme dontfloatme">
                                <div class="form-group">
                                    <label for="password" class="sr-only">Password</label>
                                    <input placeholder="Password" id="password" type="password" class="form-control input-lg">
                                </div>
                            </div>
                            <div class="col-md-6 centerme dontfloatme">
                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary " value="Register">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $('.fake-face').on('click',function(){
            $('#fake-face').val(1);
            $('#login-form').submit();
        });
    </script>
</div>

