<div class="item slide1">
    <div class="fh5co-overlay"></div>
    <div class="container slide" style="background-image:url(images/slide_1.jpg); background-repeat: no-repeat; background-color:#0633ff;background-size:375px;background-position: center;">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="fh5co-owl-text-wrap">
                    <div class="fh5co-owl-text text-center to-animate">
                        <h1 class="fh5co-lead" style="margin-top:350px;">WYS is a CHARITYGRAM</h1>
                        <h2 class="fh5co-sub-lead">That turns your best memories into social energy</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div id="fh5co-features">
            <div class="container">
                <div class="row text-center">
                    <div class="col-md-8 col-md-offset-2">
                        <h2 class="fh5co-section-lead">Be WYS to us means:</h2>
                        <h3 class="fh5co-section-sub-lead">To create a sustainable and engaging charitygram to send and share moments of happiness</h3>
                    </div>
                    <div class="fh5co-spacer fh5co-spacer-md"></div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6 fh5co-feature-border">
                        <div class="fh5co-feature">
                            <div class="fh5co-feature-icon to-animate">
                                <i class="icon-infinity"></i>
                            </div>
                            <div class="fh5co-feature-text">
                                <h3>How do you Waste Your Soul?</h3>
                                <p>Every moment of happiness you share is also a microdonation</p>
                                <p><a href="#">Read more</a></p>
                            </div>
                        </div>
                        <div class="fh5co-feature no-border">
                            <div class="fh5co-feature-icon to-animate">
                                <i class="icon-heart2"></i>
                            </div>
                            <div class="fh5co-feature-text">
                                <h3>User Satisfaction</h3>
                                <p>Problem with today's donation model is that it's synonym of hunger, war and sadness
</p>
                                <p><a href="#">Read more</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="fh5co-feature">
                            <div class="fh5co-feature-icon to-animate">
                                <i class="icon-microphone"></i>
                            </div>
                            <div class="fh5co-feature-text">
                                <h3>Spread your Happy Voice</h3>
                                <p>Be part and rely on a community and recognizable ambassadors</p>
                                <p><a href="#">Read more</a></p>
                            </div>
                        </div>
                        <div class="fh5co-feature no-border">
                            <div class="fh5co-feature-icon to-animate">
                                <i class="icon-clock2"></i>
                            </div>
                            <div class="fh5co-feature-text">
                                <h3>Right now</h3>
                                <p>Close the gap between your happy moment and your sharing</p>
                                <p><a href="#">Read more</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Features -->


        <div class="fh5co-spacer fh5co-spacer-lg"></div>
        <!-- Products -->
        <div class="container" id="fh5co-products">
            <div class="row text-left">
                <div class="col-md-8">
                    <h2 class="fh5co-section-lead">What We Do</h2>
                    <h3 class="fh5co-section-sub-lead">Our mission is to empower every happy people on this planet to share his moment and give a deeper value to it by binding a donation to a charity program.</h3>
                </div>
                <div class="fh5co-spacer fh5co-spacer-md"></div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 fh5co-mb30">
                    <div class="fh5co-product">
                        <img src="images/slide_1.jpg" alt="FREEHTML5.co Free HTML5 Template Bootstrap" class="img-responsive img-rounded to-animate">
                        <h4>Fesivals</h4>
                        <p>WYS works side by side with Music Festivals and selected NGOs. We believe the good vibes and the good moments you experience at live events are catalitic to the sharing.</p>
                        <p><a href="#">Read more</a></p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 fh5co-mb30">
                    <div class="fh5co-product">
                        <img src="images/slide_2.jpg" alt="FREEHTML5.co Free HTML5 Template Bootstrap" class="img-responsive img-rounded to-animate">
                        <h4>NGOs</h4>
                        <p>We have met a number of NGO partners which we will present to you for your Happinsee Sharing. They have a list of people to help which needs your part.</p>
                        <p><a href="#">Read more</a></p>
                    </div>
                </div>
                <div class="visible-sm-block visible-xs-block clearfix"></div>
                <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 fh5co-mb30">
                    <div class="fh5co-product">
                        <img src="images/slide_3.jpg" alt="FREEHTML5.co Free HTML5 Template Bootstrap" class="img-responsive img-rounded to-animate">
                        <h4>Socials</h4>
                        <p>After you have shared, post on the socials your official and watermarked picture, make your voice heard and spread the cause.</p>
                        <p><a href="#">Read more</a></p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 fh5co-mb30">
                    <div class="fh5co-product">
                        <img src="images/slide_4.jpg" alt="FREEHTML5.co Free HTML5 Template Bootstrap" class="img-responsive img-rounded to-animate">
                        <h4>Watermark</h4>
                        <p>The logo we apply on your pictures represents the drop of Happiness and good will that you put in the sharing.</p>
                        <p><a href="#">Read more</a></p>
                    </div>
                </div>

            </div>
        </div>
        <!-- Products -->
        <div class="fh5co-spacer fh5co-spacer-lg"></div>

        <div id="fh5co-clients">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 fh5co-client-logo text-center to-animate"><img src="images/client_1.png" alt="FREEHTML5.co Free HTML5 Bootstrap Template" class="img-responsive"></div>
                    <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 fh5co-client-logo text-center to-animate"><img src="images/client_2.png" alt="FREEHTML5.co Free HTML5 Bootstrap Template" class="img-responsive"></div>
                    <div class="visible-sm-block visible-xs-block clearfix"></div>
                    <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 fh5co-client-logo text-center to-animate"><img src="images/client_3.png" alt="FREEHTML5.co Free HTML5 Bootstrap Template" class="img-responsive"></div>
                    <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 fh5co-client-logo text-center to-animate"><img src="images/client_4.png" alt="FREEHTML5.co Free HTML5 Bootstrap Template" class="img-responsive"></div>
                </div>
            </div>
        </div>

        <div class="fh5co-bg-section" style="background-image: url(images/slide_2.jpg); background-attachment: fixed;">
            <div class="fh5co-overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="fh5co-hero-wrap">
                            <div class="fh5co-hero-intro text-center">
                                <h1 class="fh5co-lead" style="text-color:black"><span class="quo">&ldquo;</span>Be Happy. Share your Happiness. Send Happiness!<span class="quo">&rdquo;</span></h1>
                                <p class="author">&mdash; <cite>WasteYourSoul</cite></p> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>