<div class="item slide2">
    <div class="fh5co-overlay"></div>
    <div class="container slide" style="background-image:url(images/slide_2.jpg); background-repeat: no-repeat; background-color:#0633ff;background-size:375px;background-position: center;">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="fh5co-owl-text-wrap">
                    <div class="fh5co-owl-text text-center to-animate">
                        <h1 class="fh5co-lead" style="margin-top:400px;">NOW you can give VALUE</h1>
                        <h2 class="fh5co-sub-lead">To your best pics and videos</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div id="fh5co-features">
            <div class="container">
                <div class="row text-center">
                    <div class="col-md-8 col-md-offset-2">
                        <h2 class="fh5co-section-lead">With WYS you can:</h2>
                        <h3 class="fh5co-section-sub-lead">
                        > Give value to your best memories <br/>
                            > Be part of our community <br/>
                            > Involve your friends <br/>
                            > Check the impact your donations are creating <br/>
                            > Get news from the movement

                        </h3>
                    </div>
                    <div class="fh5co-spacer fh5co-spacer-md"></div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6 fh5co-feature-border">
                        <div class="fh5co-feature">
                            <div class="fh5co-feature-icon to-animate">
                                <i class="icon-infinity"></i>
                            </div>
                            <div class="fh5co-feature-text">
                                <h3>Send in 3 Steps!</h3>
                                <p>Sharing is easy
                                </p>
                                <p><a href="#">Read more</a></p>
                            </div>
                        </div>
                        

                        <div class="fh5co-feature no-border">
                            <div class="fh5co-feature-icon to-animate">
                                <i class="icon-paypal"></i>
                            </div>
                            <div class="fh5co-feature-text">
                                <h3>Step 2</h3>
                                <p>
                                choose the value you want to send.
                                </p>
                                <p><a href="#">Read more</a></p>
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-md-6 col-sm-6">

                        <div class="fh5co-feature">
                            <div class="fh5co-feature-icon to-animate">
                                <i class="icon-camera"></i>
                            </div>
                            <div class="fh5co-feature-text">
                                <h3>Step 1</h3>
                                <p> 
                                choose a picture you love  
                                </p>
                                <p><a href="#">Read more</a></p>
                            </div>
                        </div>
                        
                        
                        <div class="fh5co-feature no-border">
                            <div class="fh5co-feature-icon to-animate">
                                <i class="icon-share-alternitive"></i>
                            </div>
                            <div class="fh5co-feature-text">
                                <h3>Step 3</h3>
                                <p>share your pic with your community (in/out WYS) and send your donation straight to who needs.</p>
                                <p><a href="#">Read more</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>