<?php
global $_service;
$_config = $_service->get('config');
?>
<div id='topFooterOuterWrapper'>
    <div id='topFooterWrapper'>
        <div class='container'>
            <div class='row'>
                <div class='col-md-5 col-sm-4'>
                    <a href='http://www.cri.it/flex/cm/pages/ServeBLOB.php/L/IT/IDPagina/20126' title='La riforma della CRI'>La riforma della CRI &raquo;</a>
                    <a href='http://www.cri.it/elezionicri2016' title='Elezioni 2016'>Elezioni 2016 &raquo;</a>
                    <a href='http://www.cri.it/flex/cm/pages/ServeBLOB.php/L/IT/IDPagina/43' title='Eventi e Comunicazione'>Eventi e Comunicazione &raquo;</a>
                    <!-- <a href='http://www.cri.it/flex/cm/pages/adm.v2/communityUserEdit.php/L/IT/A/N?frmNewsletterEmail=Iscrizione+newsletter&R=nl' title='Iscriviti alla Newsletter'>Iscriviti alla Newsletter &raquo;</a> -->
                    <div id='nwlSubscribeFooter'>
                        <!--
                        <form action='http://www.cri.it/flex/cm/pages/adm.v2/communityUserEdit.php/L/IT/A/N'>
                           <fieldset>
                                <label for='frmNewsletterEmail'><span class='goAway'>inserisci il tuo indirizzo email e premi invia</span></label>
                                <input title='inserisci il tuo indirizzo email' type='text' name='frmNewsletterEmail' id='frmNewsletterEmail' value='Iscrizione newsletter' />
                                <input title='invia' type='submit' value='Iscrizione Newsletter' />
                                <input type='hidden' value='nl' name='R' />
                           </fieldset>
                        </form>
                        -->
                    </div>
                </div>
                <div class='col-md-4 col-sm-4 secondColumnTopFooter'>

                    <a href='https://gaia.cri.it/' target="_blank" title='Gestionale GAIA'>Gestionale GAIA &raquo;</a>
                    <!-- <a href='http://www.cri.it/flex/cm/pages/ServeBLOB.php/L/IT/IDPagina/16084' title='Patrocini'>Patrocini &raquo;</a> -->
                    <!-- <a href='http://www.cri.it/flex/cm/pages/ServeBLOB.php/L/IT/IDPagina/21343' title='Press Kit'>Press Kit &raquo;</a> -->
                    <a href='http://www.cri.it/intranet' title='Intranet'>Intranet &raquo;</a>
                    <a href='http://www.cri.it/linkutili' title='Link Utili'>Link Utili &raquo;</a>
                </div>
                <div class='col-md-3 col-sm-4 thirdColumnTopFooter'>
                    <a href='<?php echo $_config['facebook_profile'];?>' class='target_blank' title='Facebook' id='fbFollow'>Facebook &raquo;</a>
                    <a href='<?php echo $_config['twitter_profile'];?>' title='Twitter' class='target_blank' id='twFollow'>Twitter &raquo;</a>
                    <a href='<?php echo $_config['youtube_profile'];?>' class='target_blank' title='YouTube' id='ytFollow'>YouTube &raquo;</a>
                    <a href='<?php echo $_config['instagram_profile'];?>' class='target_blank' title='Instagram' id='inFollow'>Instagram &raquo;</a>
                </div>
            </div>
        </div> <!-- /container -->
    </div> <!-- /topfooterwrapper -->
</div>

<!-- /topfooterwrapper -->
<div class='utilityMenu2'>Copyright © <?php echo date('Y');?> All Rights Reserved - Associazione Italiana della Croce Rossa -
    <a href='contatti' title='Dove Trovarci'><?php echo $_config['address'];?></a> -
    <a href="tel:<?php echo $_config['phone'];?>">Tel. <?php echo $_config['phone'];?></a>
</div>