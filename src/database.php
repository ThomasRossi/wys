<?php
global $_service;
$env = $_service->get('config')['env'];

/* Constants definition */

if($env == 'PROD')
{
    /* Prod settings */
    define('DB_HOST','54.191.179.222');
    define('DB_PORT','3306');
    define('DB_USER','root');
    define('DB_PSWD','Wasteyoursoul');
    define('DB_NAME','wys');
} else {
    /* Devel settings */
    define('DB_HOST','localhost');
    define('DB_PORT','3306');
    define('DB_USER','root');
    define('DB_PSWD','root');
    define('DB_NAME','wys');
}

define('DB_DSN', 'mysql:host='.DB_HOST.';port='.DB_PORT.';dbname='.DB_NAME);

/* Database connector */
$_db = new \DB\SQL(DB_DSN, DB_USER, DB_PSWD);
$_service->set('db', $_db);