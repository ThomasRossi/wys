<?php

/* Defining routes */
$_routes = $_db->exec('SELECT `path`, `page` FROM `route`');

foreach($_routes as $route)
{
    $f3->route("GET {$route['path']}",
        function() use ($route, $f3) {
            ob_start();
            $tpl = include PAGES.$route['page'];

            if(!$tpl)
                $f3->error(404);

            $out = ob_get_contents();
            ob_end_clean();
            echo $out;
        }
    );

    $f3->route("POST {$route['path']}",
        function() use ($route, $f3) {
            ob_start();
            $tpl = include PAGES.$route['page'];

            if(!$tpl)
                $f3->error(404);

            $out = ob_get_contents();
            ob_end_clean();
            echo $out;
        }
    );
}