<?php

class Service
{
    protected $props;

    public function set($prop, $value)
    {
        $this->props[$prop] = $value;
    }

    public function get($prop)
    {
        return $this->props[$prop];
    }

    public function loadTpl($name, $params = null)
    {
        if(!is_null($params))
            foreach($params as $k => $v)
            {
                if(isset($k))
                    $_var_predefined[md5($k)] = $v;

                $$k = $v;
            }

        include "tpl/$name.php";

        if(!is_null($params))
            foreach($params as $k => $v)
            {
                if(isset($_var_predefined[md5($k)]))
                {
                    $$k = $_var_predefined[md5($k)];
                    unset($_var_predefined[md5($k)]);
                } else {
                    unset($$k);
                }
            }
    }

    public function loadModule($name, &$var = null)
    {
        include MODULES."/$name.php";

        $name = ucfirst($name);

        if(is_null($var))
            return new $name;
        else
            $var = new $name;
    }

}