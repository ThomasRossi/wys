<?php
global $_service;
$_config = $_service->get('config');

if($_SESSION['is_logged'])
{
    header($_config['website_url'].'/dashboard');
}
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <?php $_service->loadTpl('header'); ?>

    <?php $_service->loadTpl('assets_js'); ?>
</head>
<body>
<!-- NAVBAR -->
<header id="fh5co-header" role="banner">
    <?php $_service->loadTpl('navbar'); ?>
</header>
<!-- END .header -->

<!-- SLIDE -->
<div class="fh5co-slider">
    <!-- CAROUSEL START -->
    <div class="owl-carousel owl-carousel-fullwidth">

        <?php $_service->loadTpl('slide1'); ?>

        <?php $_service->loadTpl('slide2'); ?>

        <?php $_service->loadTpl('slide3'); ?>

    </div>
    <!-- CAROUSEL END -->
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Login</h4>
            </div>
            <div class="modal-body">
                <form action="#" method="post">
                    <br />
                    <div class="col-md-6 centerme dontfloatme">
                        <div class="form-group">
                            <label for="name" class="sr-only">Nickname</label>
                            <input placeholder="Username or Email" id="nickname" type="text" class="form-control input-lg">
                        </div>
                    </div>
                    <div class="col-md-6 centerme dontfloatme">
                        <div class="form-group">
                            <label for="email" class="sr-only">Password</label>
                            <input placeholder="Password" id="password" type="text" class="form-control input-lg">
                        </div>
                    </div>
                    <div class="col-md-6 centerme dontfloatme">
                        <div class="form-group">
                            <input type="submit" style="margin-left: 33%;" class="btn btn-primary" value="Login">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END SLIDE -->
<!-- MAIN BODY -->
<div id="fh5co-main">

</div>
<!-- END MAIN BODY -->
<!-- FOOTER -->
<footer id="fh5co-footer">

</footer>
<!-- END FOOTER -->

</body>
</html>