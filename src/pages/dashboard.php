<?php
global $_service;
$_config = $_service->get('config');

/*if(is_null($_SESSION['is_logged']) || !$_SESSION['is_logged'])
{
    header('Location: '.$_config['website_url']);
}*/
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <?php $_service->loadTpl('header'); ?>

    <?php $_service->loadTpl('assets_js'); ?>
</head>
<body>
<!-- NAVBAR -->
<header id="fh5co-header" role="banner">
    <?php $_service->loadTpl('navbar'); ?>
</header>
<!-- END .header -->

<!-- SLIDE -->
<div class="fh5co-slider">
    <!-- CAROUSEL START -->
    <div class="owl-carousel owl-carousel-fullwidth">

        <?php $_service->loadTpl('slide4'); ?>

        <?php $_service->loadTpl('slide5'); ?>

        <?php $_service->loadTpl('slide3'); ?>

    </div>
    <!-- CAROUSEL END -->
</div>
<div class="modal fade" id="payModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-paypal" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body">
                <div class="embed-responsive embed-responsive-4by3">
                    <iframe class="embed-responsive-item" src="http://54.191.179.222/wys/braintree_php_example/public_html/index.php"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END SLIDE -->
<!-- MAIN BODY -->
<div id="fh5co-main">

</div>
<!-- END MAIN BODY -->
<!-- FOOTER -->
<footer id="fh5co-footer">

</footer>
<!-- END FOOTER -->

</body>
</html>