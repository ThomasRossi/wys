<?php
global $_service;

/* Configurations */
$_config = array();
foreach($_db->exec('SELECT `key`, `value` FROM `config`') as $c)
{
    $_config[$c['key']] = $c['value'];
}
unset($c);
$_service->set('config', $_config);