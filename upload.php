<?php
error_reporting(0);
$target_path = basename($_FILES['myfile']['name']);

if(!empty($target_path)) {
    move_uploaded_file($_FILES['myfile']['tmp_name'], 'uploads/' . $target_path);

    $overlay = new Imagick('images/mark.png');
    $image = new Imagick('uploads/' . $target_path);

    $image->setImageColorspace($overlay->getImageColorspace() );
    $image->compositeImage($overlay, Imagick::COMPOSITE_DEFAULT, 10, 10);
    $image->writeImage('uploads/' . $target_path); //replace original background

    $overlay->destroy();
    $image->destroy();

    echo 'uploads/' . $target_path;
}




