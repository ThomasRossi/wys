<?php

$overlay = new Imagick('mark.png');
$image = new Imagick('image1.jpg');

$image->setImageColorspace($overlay->getImageColorspace() ); 
$image->compositeImage($overlay, Imagick::COMPOSITE_DEFAULT, 10, 10);
$image->writeImage('out1.jpg'); //replace original background

$overlay->destroy();
$image->destroy();

?>
